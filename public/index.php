<?php

require(__DIR__ . '/../vendor/autoload.php');

$fw = Base::instance();

$fw->set('AUTOLOAD', __DIR__ . '/../App/Controllers/');

$fw->route('POST /subscribe', 'SubscriptionController->subscribe');

$fw->run();